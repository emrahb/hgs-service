FROM openjdk:8
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/hgs-service/hgs-service.jar"]

ADD target/hgs-service.jar /usr/share/hgs-service/hgs-service.jar